(function () {
    var file_uploaded_sizes;
    var file_total_size;
    var file_uploaded_status;
    var uploading = false;

    function $id(id) {
        return document.getElementById(id);
    }

    function Output(msg) {
        var m = $id("messages");
        m.innerHTML = msg + m.innerHTML;
    }

    function FileSelectHandler(e) {
        if (uploading) {
            return;
        }
        uploading = true;

        var files = e.target.files || e.dataTransfer.files;

        file_uploaded_sizes = [];
        file_uploaded_status = [];
        file_total_size = 0;
        for (var i = 0, f; f = files[i]; i++) {
            file_uploaded_sizes[i] = 0;
            file_uploaded_status[i] = 0;
            file_total_size += f.size;

            ParseFile(f);
            UploadFile(i, f);
        }

    }

    function ParseFile(file) {
        Output(
            "<p>Upload started: <strong>" + file.name +
            "</strong> size: <strong>" + file.size +
            "</strong> bytes</p>"
        );
    }

    function is_upload_complete() {
        for (var i in file_uploaded_status) {
            if (file_uploaded_status[i] == 0) return false;
        }

        return true;
    }

    function get_uploaded_bytes() {
        var uploaded_bytes = 0;
        for (var i in file_uploaded_sizes) {
            uploaded_bytes += file_uploaded_sizes[i];
        }

        return uploaded_bytes;
    }

    function set_progress(percent) {
        var progress = $id("progress");

        if (progress && (progress.value !== undefined)) {
            progress.value = percent;
        }
    }

    function UploadFile(index, file) {
        var xhr = new XMLHttpRequest();
        if (xhr.upload && file.size <= $id("MAX_FILE_SIZE").value) {
            xhr.upload.addEventListener("progress", function (e) {
                file_uploaded_sizes[index] = e.loaded;

                var pc = parseInt(get_uploaded_bytes() / file_total_size * 96);
                set_progress(pc);
            }, false);

            // file received/failed
            xhr.onreadystatechange = function (e) {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        file_uploaded_status[index] = 1;
                        if (is_upload_complete()) {
                            set_progress(100);
                            uploading = false;
                            $id('upload').reset();
                        }
                        Output('<p>' + xhr.responseText + ' uploaded.</p>');
                    }
                }
            };

            xhr.open("POST", $id("upload").action, true);
            xhr.setRequestHeader("X-FILENAME", file.name);
            xhr.send(file);
        }
    }

    function Init() {
        var fileselect = $id("fileselect"),
            submitbutton = $id("submitbutton");

        fileselect.addEventListener("change", FileSelectHandler, false);

        var xhr = new XMLHttpRequest();
        if (xhr.upload) {
            submitbutton.style.display = "none";
        }
    }
    if (window.File && window.FileList && window.FileReader) {
        Init();
    }
})();
