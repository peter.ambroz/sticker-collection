<?php

namespace App\Tests\Service;

use App\Service\FileData;
use PHPUnit\Framework\TestCase;

class FileDataTest extends TestCase
{
    private FileData $fd;

    protected function setUp()
    {
        $fd = new FileData();
        $fd->filename = 'file.jpg';
        $fd->storageDir = 'data';
        $fd->relativeDir = '00';
        $this->fd = $fd;
    }

    public function testGetFullName()
    {
        $this->assertEquals('data/00/file.jpg', $this->fd->getFullName());
    }

    public function testGetDirectory()
    {
        $this->assertEquals('data/00', $this->fd->getDirectory());
    }

    public function testGetRelName()
    {
        $this->assertEquals('00/file.jpg', $this->fd->getRelName());
    }
}
