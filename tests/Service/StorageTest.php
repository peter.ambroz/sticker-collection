<?php

namespace App\Tests\Service;

use App\Service\Storage;
use PHPUnit\Framework\TestCase;

class StorageTest extends TestCase
{
    public function testGetFile()
    {
        $storage = new Storage('/tmp');
        $file = $storage->getFile('jpg')->getFullName();
        $this->assertStringStartsWith('/tmp/', $file);
        $this->assertStringEndsWith('.jpg', $file);
        $this->assertEquals(substr($file, 5, 2), substr($file, 8, 2));
        $this->assertEquals(32, strlen($file));
        $this->assertFileExists($file);

        unlink($file);
    }
}
