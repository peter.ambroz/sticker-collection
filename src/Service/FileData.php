<?php

declare(strict_types=1);

namespace App\Service;

class FileData
{
    public string $storageDir;
    public string $relativeDir;
    public string $filename;

    public function getFullName(): string
    {
        return sprintf('%s/%s/%s', $this->storageDir, $this->relativeDir, $this->filename);
    }

    public function getRelName(): string
    {
        return sprintf('%s/%s', $this->relativeDir, $this->filename);
    }

    public function getDirectory(): string
    {
        return sprintf('%s/%s', $this->storageDir, $this->relativeDir);
    }
}
