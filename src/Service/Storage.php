<?php

declare(strict_types=1);

namespace App\Service;

class Storage
{
    private string $prefix;

    public function __construct(string $storagePath)
    {
        $this->prefix = $storagePath;
    }

    public function getFile(string $suffix): FileData
    {
        $data = new FileData();
        $data->storageDir = $this->prefix;
        do {
            $file = bin2hex(random_bytes(10));
            $data->relativeDir = substr($file, 0, 2);
            $data->filename = sprintf('%s.%s', $file, $suffix);
        } while (file_exists($data->getFullName()));

        if (!file_exists($data->getDirectory())) {
            mkdir($data->getDirectory());
        }

        touch($data->getFullName());
        return $data;
    }
}
