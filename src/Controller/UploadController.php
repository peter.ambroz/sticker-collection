<?php

namespace App\Controller;

use App\Entity\Sticker;
use App\Repository\StickerRepository;
use App\Service\Storage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/upload")
 */
class UploadController extends AbstractController
{
    /**
     * @Route("/", name="upload", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        return $this->render('upload/index.html.twig', ['success' => $request->query->get('success', '0')]);
    }

    /**
     * @Route("/", name="upload_post", methods={"POST"})
     */
    public function upload(Request $request, Storage $storage, StickerRepository $stickerRepository): Response
    {
        if ($request->files->has('fileselect')) {
            /** @var UploadedFile[] $files */
            $files = $request->files->get('fileselect');
            foreach ($files as $file) {
                $target = $storage->getFile($file->getClientOriginalExtension());
                $file->move($target->getDirectory(), $target->filename);

                $sticker = (new Sticker())->setImagePath($target->getRelName());
                $stickerRepository->add($sticker);
            }
            return $this->redirectToRoute('upload', ['success' => '1']);
        }

        if ($request->headers->has('X-FILENAME')) {
            $orig = substr($request->headers->get('X-FILENAME'), 0, 256);
            $parts = explode('.', $orig);
            if (count($parts) > 1) {
                $ext = $parts[count($parts) - 1];
            } else {
                $ext = 'jpg';
            }

            $target = $storage->getFile($ext);
            file_put_contents($target->getFullName(), $request->getContent());

            $sticker = (new Sticker())->setImagePath($target->getRelName());
            $stickerRepository->add($sticker);

            return new Response('OK/xhr ' . $target->filename);
        }

        throw new \BadMethodCallException('Use XHR file upload');
    }
}
