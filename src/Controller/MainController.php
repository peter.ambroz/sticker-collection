<?php

namespace App\Controller;

use App\Entity\Sticker;
use App\Repository\StickerRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    private const PAGESIZE_MIN = 5;
    private const PAGESIZE_MAX = 100;
    private const PAGESIZE_DEFAULT = 40;

    /**
     * @Route("/", name="main")
     */
    public function index(StickerRepository $stickerRepository, Request $request): Response
    {
        $q = $request->query->get('q');
        $limit = max(
            min(
                $request->query->get('_size', self::PAGESIZE_DEFAULT),
                self::PAGESIZE_MAX
            ),
            self::PAGESIZE_MIN
        );
        $page = $request->query->get('_page', 0);
        $offset = $page * $limit;

        $count = $stickerRepository->count([]);
        if ($offset >= $count) {
            $offset = 0;
            $page = 0;
        }

        $lastPage = (int)ceil($count / $limit) - 1;

        if (empty($q)) {
            $stickers = $stickerRepository->findBy([], ['created' => 'DESC'], $limit, $offset);
        } else {
            $stickers = $stickerRepository->findBy([], ['created' => 'DESC'], $limit, $offset);
        }

        return $this->render('main/index.html.twig', [
            'stickers' => $stickers,
            'count' => $count,
            'page' => $page,
            'limit' => $limit,
            'prev' => max($page - 1, 0),
            'next' => min($page + 1, $lastPage),
            'last' => $lastPage,
            'query' => $q,
            'datadir' => $this->getParameter('storage_path_rel') . '/',
        ]);
    }

    /**
     * @Route("/s/{id}", name="sticker", requirements={"id":"\d+"})
     */
    public function sticker(Sticker $sticker): Response
    {
        return $this->render('main/sticker.html.twig', [
            'sticker' => $sticker,
            'datadir' => '/' . $this->getParameter('storage_path_rel') . '/',
        ]);
    }
}
