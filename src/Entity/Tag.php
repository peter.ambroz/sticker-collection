<?php

namespace App\Entity;

use App\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TagRepository::class)
 * @ORM\Table(indexes={
 *     @ORM\Index(name="idx_name", columns={"name"}),
 *     @ORM\Index(name="idx_type", columns={"type"})
 * })
 */
class Tag
{
    public const TYPE_BREWERY = 'brewery';
    public const TYPE_BEER = 'beer';
    public const TYPE_TYPE = 'type';
    public const TYPE_ALC = 'alc';
    public const TYPE_OTHER = 'other';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $type;

    /**
     * @ORM\ManyToMany(targetEntity=Sticker::class, mappedBy="tags")
     */
    private Collection $stickers;

    public function __construct()
    {
        $this->type = self::TYPE_OTHER;
        $this->stickers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Sticker[]
     */
    public function getStickers(): Collection
    {
        return $this->stickers;
    }
}
