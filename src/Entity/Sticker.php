<?php

namespace App\Entity;

use App\Repository\StickerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StickerRepository::class)
 * @ORM\Table(indexes={
 *     @ORM\Index(name="idx_created", columns={"created"})
 * })
 */
class Sticker
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $imagePath;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $created;

    /**
     * @ORM\Column(type="text")
     */
    private string $comment;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="stickers")
     */
    private Collection $tags;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private ?User $tagAuthor;

    public function __construct()
    {
        $this->created = new \DateTimeImmutable();
        $this->comment = '';
        $this->tagAuthor = null;
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    public function getCreated(): ?\DateTimeImmutable
    {
        return $this->created;
    }

    public function setCreated(\DateTimeImmutable $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->tags->removeElement($tag);

        return $this;
    }

    public function getTagAuthor(): ?User
    {
        return $this->tagAuthor;
    }

    public function setTagAuthor(?User $tagAuthor): self
    {
        $this->tagAuthor = $tagAuthor;

        return $this;
    }
}
